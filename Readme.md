# EWM


## TODO: Behavior Specific
- On launch a program if it is closed return to the last active workspace. Use case launching image visor from a file manager that take you to another workspace, closing it should return to the last workspace where the file manager was.

## TODO
- [ ] Define configuration files
- [ ] UI for configuration
  - It needs to be communicate with the compositor through wayland protocols.
  - Qt licensing is not an option
  - GTK could be used.
  - Conrod and all rust UI friends
    - It needs some declarative API to build Apps
  - Research about damage rendering, and its cost to be implemented
  - Provide some extra functionality to modify internal UI with the wayland protocols. Thinking on plugins.
- [ ] Touch controls

## Use Cases

- Remote Control Administration, good interaction with several monitors
- 