<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <xsl:for-each select="protocol"/>
                <h1><xsl:value-of select="/@name"/></h1>
                <p><xsl:value-of select="/copyright"/></p>
                <xsl:for-each select="interface">
                    <h2><xsl:value-of select="/@name"/> <b>Version:<xsl:value-of select="/@version"/></b></h2>
                    <p><xsl:value-of select="description"/></p>
                    <xsl:for-each select="*">
                        <xsl:if test="/enum">

                        </xsl:if>
                        <xsl:if test="/request">
                            <h3>Request (Client -> Server)</h3>
                        </xsl:if>
                        <xsl:if test="/event">
                        <h3>Event (Server -> Client)</h3>
                        </xsl:if>
                    </xsl:for-each>


                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>