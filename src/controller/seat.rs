use std::collections::HashSet;

use wlroots::{input::keyboard::Modifiers, seat, wlroots_dehandle};

pub struct SeatHandler;

impl seat::Handler for SeatHandler {}

/// Inputs associate to a seat and this seat it can be associate with the window manager by
/// workspaces
pub struct Seat {
    /// Handle seat
    pub seat_handle: wlroots::seat::Handle,
    /// Keyboards associate to seat
    pub keyboards: HashSet<wlroots::input::keyboard::Handle>,
    /// Pointers associate to seat
    pub pointers: HashSet<wlroots::input::pointer::Handle>,
    /// Touch devices associate to seat
    pub touch: HashSet<wlroots::input::touch::Handle>,
    /// Switches associate to seat
    pub switches: HashSet<wlroots::input::switch::Handle>,
    /// Tablet pads associate to seat
    pub tablet_pad: HashSet<wlroots::input::tablet_pad::Handle>,
    /// Tablet tools associate to seat
    pub tablet_tool: HashSet<wlroots::input::tablet_tool::Handle>,
}

impl Seat {
    pub fn new() -> Self {
        Self {
            seat_handle: seat::Handle::new(),
            keyboards: HashSet::new(),
            pointers: HashSet::new(),
            touch: HashSet::new(),
            switches: HashSet::new(),
            tablet_pad: HashSet::new(),
            tablet_tool: HashSet::new(),
        }
    }

    #[wlroots_dehandle]
    pub fn get_keyboard_info(&mut self) -> (Vec<u32>, Modifiers) {
        match self.keyboards.iter().next() {
            None => (vec![], Modifiers::default()),
            Some(keyboard_handle) => {
                #[dehandle]
                let keyboard = keyboard_handle;
                (keyboard.keycodes(), keyboard.get_modifier_masks())
            }
        }
    }
}
