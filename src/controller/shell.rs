use wlroots::{shell::xdg_shell, shell::xdg_shell_v6, wlroots_dehandle};

pub mod xdg;

#[derive(Clone, Hash, Eq, PartialEq)]
pub enum Shell {
    Xdg(xdg_shell::Handle),
    XdgV6(xdg_shell_v6::Handle),
}

impl Into<Shell> for xdg_shell::Handle {
    fn into(self) -> Shell {
        Shell::Xdg(self)
    }
}

impl Shell {
    #[wlroots_dehandle]
    pub fn surface(&self) -> wlroots::surface::Handle {
        match self {
            // TODO:
            Shell::Xdg(xdg) => {
                #[dehandle]
                let xdg = xdg;
                xdg.surface()
            }
            Shell::XdgV6(xdg) => {
                #[dehandle]
                let xdg = xdg;
                xdg.surface()
            }
        }
    }

    #[wlroots_dehandle]
    pub fn set_activated(&self, activated: bool) {
        match self {
            // TODO:
            Shell::Xdg(xdg) => {
                use wlroots::shell::xdg_shell::ShellState::*;
                #[dehandle]
                let xdg = xdg;
                match xdg.state() {
                    Some(TopLevel(toplevel)) => {
                        toplevel.set_activated(activated);
                    }
                    _ => {}
                }
            }
            Shell::XdgV6(xdg) => {
                use wlroots::shell::xdg_shell_v6::ShellState::*;
                #[dehandle]
                let xdg = xdg;
                match xdg.state() {
                    Some(TopLevel(toplevel)) => {
                        toplevel.set_activated(activated);
                    }
                    _ => {}
                }
            }
        }
    }
}
