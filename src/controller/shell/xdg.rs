use crate::controller::CompositorState;

use wlroots::{compositor, shell::xdg_shell, surface, wlroots_dehandle};

struct SurfaceHandler;

impl surface::Handler for SurfaceHandler {}

struct XdgShellHandler;

impl xdg_shell::Handler for XdgShellHandler {
    #[wlroots_dehandle]
    fn map_request(
        &mut self,
        compositor_handle: compositor::Handle,
        _surface_handle: surface::Handle,
        shell_handle: xdg_shell::Handle,
    ) {
        #[dehandle]
        let compositor = compositor_handle;
        let compositor_state: &mut CompositorState = compositor.downcast();
        compositor_state
            .window_manager
            .show(shell_handle.into());
    }

    #[wlroots_dehandle]
    fn unmap_request(
        &mut self,
        compositor_handle: compositor::Handle,
        _surface_handle: surface::Handle,
        shell_handle: xdg_shell::Handle,
    ) {
        #[dehandle]
        let compositor = compositor_handle;
        let compositor_state: &mut CompositorState = compositor.downcast();
        compositor_state
            .window_manager
            .hide(shell_handle.into());
    }

    #[wlroots_dehandle]
    fn destroyed(
        &mut self,
        compositor_handle: compositor::Handle,
        shell_handle: xdg_shell::Handle,
    ) {
        #[dehandle]
        let compositor = compositor_handle;
        let compositor_state: &mut CompositorState = compositor.downcast();
        compositor_state
            .window_manager
            .hide(shell_handle.into());
    }
}

#[wlroots_dehandle]
pub fn xdg_new_surface(
    compositor_handle: compositor::Handle,
    shell_handle: xdg_shell::Handle,
) -> (
    Option<Box<xdg_shell::Handler>>,
    Option<Box<surface::Handler>>,
) {
    #[dehandle]
    let compositor = compositor_handle;
    let compositor_state: &mut CompositorState = compositor.downcast();
    compositor_state
        .window_manager
        .insert_shell(shell_handle.clone().into());
    (
        Some(Box::new(XdgShellHandler) as _),
        Some(Box::new(SurfaceHandler) as _),
    )
}
