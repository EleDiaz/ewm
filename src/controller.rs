pub mod inputs;
pub mod output;
pub mod seat;
pub mod shell;

use wlroots::{compositor, wlroots_dehandle};

use crate::{
    controller::{seat::Seat, shell::Shell},
    model::{input_state::InputState, windows_set::WindowsSet, config::Config},
};

pub struct CompositorState {
    pub window_manager: WindowsSet<Shell, wlroots::output::Handle>,
    pub input_state: InputState,
    pub layout: wlroots::output::layout::Handle,
    // TODO: Change to support several seats
    pub seat: Seat,

    /// TODO Checkout if there need to be with an specific seat or pointer device
    pub xcursor_manager: wlroots::cursor::xcursor::Manager,
    /// TODO Checkout if there need to be with an specific seat or pointer device
    pub cursor_handle: wlroots::cursor::Handle,
}

#[wlroots_dehandle]
pub fn setup_window_manager() -> CompositorState {
    use crate::{controller::inputs::pointer::CursorHandler, controller::output::LayoutHandler};

    use wlroots::{
        cursor::{xcursor, Cursor},
        output::layout::Layout,
    };
    let config = Config::new();

    let output_layout_handle = Layout::create(Box::new(LayoutHandler));
    // Make a sentinel seat to be filled in after the compositor is created.
    let cursor_handle = Cursor::create(Box::new(CursorHandler));
    let xcursor_manager = xcursor::Manager::create("default".to_string(), 24)
        .expect("Could not create xcursor manager");
    xcursor_manager.load(1.0);
    #[dehandle]
    let output_layout = output_layout_handle.clone();
    #[dehandle]
    let cursor = cursor_handle.clone();

    cursor.attach_output_layout(output_layout);
    CompositorState {
        window_manager: WindowsSet::new(config),
        input_state: InputState::new(),
        layout: output_layout_handle,
        xcursor_manager,
        cursor_handle,
        seat: Seat::new(),
    }
}

/// Set up the seat for the compositor.
/// Needs to be done after the compositor is already created.
pub fn setup_seat(compositor: &mut compositor::Compositor) {
    use wlroots::seat::Seat;
    let seat_handle = Seat::create(compositor, "default".into(), Box::new(seat::SeatHandler));
    let state: &mut CompositorState = compositor.data.downcast_mut().unwrap();
    state.seat.seat_handle = seat_handle;
}
