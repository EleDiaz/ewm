pub mod controller;
pub mod model;

use std::{
    env,
    process::{Command, Stdio},
};

use wlroots::{
    compositor,
    utils::log::{init_logging, WLR_DEBUG},
};

use crate::controller::{
    inputs::{keyboard::keyboard_added, pointer::pointer_added},
    output::output_added,
    setup_seat, setup_window_manager, shell
};


fn main() {
    init_logging(WLR_DEBUG, None);
    let compositor_state = setup_window_manager();
    let output_builder = wlroots::output::manager::Builder::default().output_added(output_added);
    let input_builder = wlroots::input::manager::Builder::default()
        .pointer_added(pointer_added)
        .keyboard_added(keyboard_added);
    let xdg_shell_builder = wlroots::shell::xdg_shell::manager::Builder::default()
        .surface_added(shell::xdg::xdg_new_surface);
    let mut compositor = compositor::Builder::new()
        .gles2(true)
        .wl_shm(true)
        .data_device(true)
        .input_manager(input_builder)
        .output_manager(output_builder)
        .xdg_shell_manager(xdg_shell_builder)
        .build_auto(compositor_state);
    setup_seat(&mut compositor);
    spawn_startup_command(&compositor);
    compositor.run();
}

/// Spawns a startup command, if one was provided on the command line.
fn spawn_startup_command(compositor: &compositor::Compositor) {
    let args = env::args().skip(1).collect::<Vec<_>>();
    env::set_var("WAYLAND_DISPLAY", compositor.socket_name());
    if args.len() > 0 {
        let command = args.join(" ");
        Command::new("/bin/sh")
            .arg("-c")
            .arg(command.as_str())
            .stdin(Stdio::null())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .spawn()
            .expect(&format!("Could not spawn {}", command));
    }
}
