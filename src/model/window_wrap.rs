use std::{rc::Rc, cell::RefCell};
use crate::model::workspace::Workspace;

pub struct WindowWrap<Shell, Output> {
    /// Workspace where it belongs, if there arent any workspace
    /// linked, that means the window belongs to `global_windows`
    workspace: Option<Rc<RefCell<Workspace<Shell, Output>>>>,
    status: Status,
    // Used to queue windows in right order to avoid hide important windows
    // drawing_preference: i32,
    pub shell: Shell,
    pub mapped: bool,
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

// In case of a numeric preference
// 
// pub enum DrawingPreference {
//     OverFullScreen,
//     OverFloating,
//     Normal,
// }

pub enum Status {
    Tiled(usize),
    Floating,
    Minimized,
    FullScreen
}

impl<Shell, Output> WindowWrap<Shell, Output> {
    /// TODO: Set status in function configuration
    pub fn new(shell: Shell) -> Self {
        Self {
            workspace: None,
            status: Status::Floating,
            shell: shell.into(),
            mapped: false,
            x: 0,
            y: 0,
            width: 0,
            height: 0,
        }
    }

    pub fn new_on(shell: Shell, workspace:Rc<RefCell<Workspace<Shell, Output>>>) -> Self {
        let window_wrap = Self::new(shell);
        window_wrap.workspace = Some(workspace);
        window_wrap
    }
}
