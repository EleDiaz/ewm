/// Defines split line orientation
pub enum Orientation {
    Horizontal,
    Vertical,
}
