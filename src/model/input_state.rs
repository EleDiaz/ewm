// use bit_set::BitSet;

/// Keep state of keys and link the inputs to workspaces
pub struct InputState {
    // This allows to split the focus between workspaces, and it could "trap the mouse" inside the workspace.
    // It needs a way better definition of how it should works.
    //pub workspaces: BitSet,
    // pub mode: Mode,
    pub shift_pressed: bool,
    pub ctrl_pressed: bool,
    pub clicked: bool,
}

//pub struct Mode {}

impl InputState {
    pub fn new() -> Self {
        Self {
            // mode: Mode,
            shift_pressed: false,
            ctrl_pressed: false,
            clicked: false,
        }
    }
}
