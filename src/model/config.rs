/*
 *
 * On Wayland use app id, as equivalent to class, name, title
 */

use regex::Regex;

// Workspaces
// Rules
// Layout
pub struct Config {
    workspace: Vec<Workspace>,
    /// Rules by Window Shell
    rules: Vec<Rule>,
    layout: Layout,
}

impl Config {
    pub fn new() -> Self {
        Self {
            workspace: Vec::new(),
            rules: Vec::new(),
            layout: Layout::new(),
        }
    }
}

pub struct Workspace {
    name: String,
}


pub struct Rule {
    app_id: Regex,
    title: Regex,
    // Modify which workspace insert/ scratchpad
    // Which status
    // action:
    // Decoration {}
}

pub struct Layout {
    split_proportion: f64,
    windows_tiled_insert: TiledInsert,
    tiled_gaps: Gaps,
}

impl Layout {
    pub fn new() -> Self {
        Self {
            split_proportion: 0.5,
            windows_tiled_insert: TiledInsert::Proportional,
            tiled_gaps: Gaps { gap_width: 5, dragger_width: 5,}
        }
    }
}

pub enum TiledInsert {
    Proportional
}

pub struct Gaps {
    gap_width: usize,
    dragger_width: usize,
}

pub struct Decoration {
    side: Edge,
    behavior: WindowType, // *
    looks: DecorationLook,
}

pub enum Edge {
    Top, Bottom, Left, Right
}

pub enum WindowType {
    Float, Maximize, Minimized, Tiled, SemiTiled
}

/// TODO: Use a xmonbar style to define the organization of this widget
pub struct DecorationLook {}

