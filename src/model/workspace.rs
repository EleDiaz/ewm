use std::{rc::Rc,collections::VecDeque, cell::RefCell};

use crate::model::{bsp::BSP, utils::Orientation, window_wrap::WindowWrap, windows_set::Screen};

pub struct Workspace<Shell, Output> {
    /// Screen associate to workspace
    pub screen: Option<Rc<RefCell<Screen<Output, Shell>>>>,
    /// Holds the information of how tiled windows are positioned
    tiled_info: BSP<WSplit>,
    /// Tiled Windows
    tiled: VecDeque<Rc<RefCell<WindowWrap<Shell, Output>>>>,
    /// Floating windows
    floating: VecDeque<Rc<RefCell<WindowWrap<Shell, Output>>>>,
    /// Minimized windows
    minimized: VecDeque<Rc<RefCell<WindowWrap<Shell, Output>>>>,
    /// There a full_screen window in the workspace
    full_screen: Option<Rc<RefCell<WindowWrap<Shell, Output>>>>,
}

struct WSplit {
    pub orientation: Orientation,
    pub ratio: f64,
}

impl<Shell, Output> Workspace<Shell, Output> {

    pub fn insert_window(&mut self, window: Rc<RefCell<WindowWrap<Shell, Output>>>) {
        // look at config how it should be inserted
        self.floating.push_back(window);
    }

    pub fn map_windows<F>(&mut self, mut apply: F)
    where F: FnMut(&mut Shell) {
        if let Some(window_wrap) = &mut self.full_screen {
            apply(&mut window_wrap.borrow_mut().shell)
        }
        else {
            // tiled
            //self.tiled.map
        }
        self.floating.iter().for_each(|window_wrap| {
            apply(&mut window_wrap.borrow_mut().shell)
        });
    }
}
