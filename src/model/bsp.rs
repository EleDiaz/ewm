// use log::{error, info, log};

pub enum Node<E> {
    Branch(Box<E>),
    Empty,
}

impl<E: Clone> Clone for Node<E> {
    fn clone(&self) -> Self {
        match self {
            Node::Branch(val) => Node::Branch(val.clone()),
            Node::Empty => Node::Empty,
        }
    }
}

impl<E> Node<E> {
    pub fn is_empty(&self) -> bool {
        match self {
            Node::Empty => true,
            _ => false,
        }
    }

    pub fn is_leaf(&self) -> bool {
        match self {
            Node::Leaf(_) => true,
            _ => false,
        }
    }
}

/// Normal Tree
pub struct BSP<E> {
    nodes: Vec<Node<E>>,
    max_level: usize,
}

impl<E: Clone> Clone for BSP<E> {
    fn clone(&self) -> Self {
        Self {
            nodes: self.nodes.clone(),
            max_level: self.max_level,
        }
    }
}

impl<E> Default for BSP<E> {
    fn default() -> Self {
        Self::empty()
    }
}

impl<E> BSP<E> {
    /// Creates a empty layout
    pub fn empty() -> Self {
        Self {
            nodes: vec![Node::Empty],
            max_level: 1,
        }
    }

    pub fn get(&self, ix: usize) -> Option<&E> {
        match self.nodes.get(ix) {
            Some(Node::Branch(val)) => Some(val),
            _ => None,
        }
    }

    pub fn get_mut(&mut self, ix: usize) -> Option<&mut E> {
        match self.nodes.get_mut(ix) {
            Some(Node::Branch(ref mut val)) => Some(val),
            _ => None,
        }
    }

    /// Find a node with a query a return his position into the tree
    pub fn find<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(&Node<E>) -> bool,
    {
        let mut ix = 0;
        let mut found = None;
        while ix < self.nodes.len() && found.is_none() {
            if predicate(&self.nodes[ix]) {
                found = Some(ix);
            } else {
                ix += 1;
            }
        }
        found
    }

    /// Insert a new value in the first empty node, it keeps a balanced tree
    /// O(n)
    pub fn add_balanced<F>(&mut self, edge:E) {
        match self.find(Node::is_empty) {
            Some(ix) => self.insert(ix, edge),
            None => self.insert(self.nodes.len(), edge),
        }
    }

    /// UNSAFE (could break internal structure logic)
    /// O(n) Almost O(1) except when is necesary to increase the vector
    pub fn insert(&mut self, ix: usize, edge: E) {
        let parent = BSP::<E>::get_parent(ix);
        if ix == 0 {
            self.nodes[0] = Node::Branch(Box::new(edge))
        } else {
            if self.nodes.len() <= 2 * parent + 1 {
                self.increase_resolution();
            }
            self.nodes.swap(2 * parent + 1, parent);
            self.nodes[2 * parent + 2] = Node::Branch(Box::new(elem));
            self.nodes[parent] = Node::Branch(Box::new(wedge));
        }
    }

    /// UNSAFE (could break internal structure logic)
    /// O(n) Almost O(1) except when is necesary to decrease the vector
    pub fn remove(&mut self, ix: usize) {
        let parent = BSP::<E, L>::get_parent(ix);
        if ix == 0 {
            *self = Self::empty();
        } else {
            let sibling = BSP::<E, L>::get_side_leaf(ix);
            self.nodes.swap(parent, sibling);
            self.nodes[ix] = Node::Empty;
            self.nodes[sibling] = Node::Empty;
            if self.can_decrease_resolution() {
                self.decrease_resolution()
            }
        }
    }

    /// UNSAFE (vector unbounds)
    /// Try to change two leaf in the tree, at worst case doesn't do anything
    pub fn interchange_leaf(&mut self, ix: usize, ix2: usize) {
        if self.nodes[ix].is_leaf() && self.nodes[ix].is_leaf() {
            self.nodes.swap(ix, ix2)
        }
    }

    /// Gaps, decorations, borders, draggers
    /// Build squares for each leaf and node. it share the position with `self.nodes`
    // pub fn build_squares(&self, root: Rectangle) -> Vec<Option<Rectangle>> {
    //     let mut squares = vec![Some(root)]; // same capacity as self.nodes
    //     let mut func = |node: &Node<E, L>, parent_rec_ix: usize| match node {
    //         Node::Node { proportion, split } => {
    //             let parent_rec: Rectangle = squares[parent_rec_ix].clone().expect("Parent Node");
    //             let mut fst = parent_rec.clone();
    //             let mut snd = parent_rec.clone();
    //             if *split == Split::Horizontal {
    //                 let new_height = parent_rec.height as f64 * *proportion;
    //                 fst.y = new_height.ceil() as u32;
    //                 fst.height = new_height.ceil() as u32;
    //                 snd.height = parent_rec.width - new_height.floor() as u32;
    //             } else {
    //                 let new_width = parent_rec.width as f64 * *proportion;
    //                 fst.width = new_width.ceil() as u32;
    //                 snd.width = new_width.ceil() as u32;
    //                 snd.x = parent_rec.width - new_width.floor() as u32;
    //             }
    //             squares[2 * parent_rec_ix + 1] = Some(fst);
    //             squares[2 * parent_rec_ix + 2] = Some(snd);
    //         }
    //         _ => return,
    //     };
    //     self.map_nodes_from(0, &mut |ix| func(&self.nodes[ix], ix));
    //     squares
    // }

    pub fn map_nodes_from<F>(&self, ix: usize, mapper: &mut F)
    where
        F: FnMut(usize) -> (),
    {
        match self.nodes[ix] {
            Node::Branch(_) => {
                mapper(ix);
                self.map_nodes_from(2 * ix + 1, mapper);
                self.map_nodes_from(2 * ix + 2, mapper);
            }
            _ => return,
        }
    }

    // pub fn leaf_move_up(&mut self, ix: usize) {}
    // pub fn leaf_move_down(&mut self, _ix: usize) {}
    // pub fn leaf_move_left(&mut self, _ix: usize) {}
    // pub fn leaf_move_right(&mut self, _ix: usize) {}
    // /// Return a sets with windows that can be interchangeable
    // pub fn leaf_moves(&self, _ix: usize) -> Vec<usize> {
    //     vec![]
    // }

    // pub fn look_upwards_until<F>(&self, ix: usize, predicate: F) -> usize
    // where
    //     F: Fn(&Node<W>) -> bool,
    // {
    //     if predicate(&self.nodes[ix]) {
    //         ix
    //     } else if ix != 0 {
    //         self.look_upwards_until(get_parent(ix), predicate)
    //     } else {
    //         0
    //     }
    // }

    pub fn increase_resolution(&mut self) {
        self.max_level = self.max_level << 1;
        self.nodes.reserve(self.nodes.len() + self.max_level);
        for _ in 0..self.max_level {
            self.nodes.push(Node::Empty);
        }
        // self.nodes.append(&mut vec![Box::new(Node::Empty); self.max_level]);
    }

    pub fn decrease_resolution(&mut self) {
        self.nodes.truncate(self.nodes.len() - self.max_level);
        self.max_level = self.max_level >> 1
    }

    /// Improvement: Take a number of left Leaf nodes in the last level
    pub fn can_decrease_resolution(&self) -> bool {
        self.nodes[self.nodes.len() - self.max_level..]
            .iter()
            .find(|node| !node.is_empty())
            .is_some()
    }

    pub fn get_side_leaf(ix: usize) -> usize {
        if (ix + 1) % 2 == 0 {
            ix + 1
        } else {
            ix - 1
        }
    }

    /// get index postion to parent, return -1 on top case
    pub fn get_parent(ix: usize) -> usize {
        ((ix + 1) / 2) - 1
    }
}

// impl<E, L> Iterator for BSP<E, L> {
//     type Item = L;

//     fn next()
// }